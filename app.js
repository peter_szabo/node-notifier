var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/send', function(req, res){
  res.sendFile(__dirname + '/send.html');
});

app.get('/receive', function(req, res){
  res.sendFile(__dirname + '/receive.html');
});

app.get('/jquery-2.1.4.min.js', function(req, res){
  res.sendFile(__dirname + '/jquery-2.1.4.min.js');
});

io.on('connection', function(socket){
  console.log('a user connected');
  
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  
  socket.on('notify', function(msg){
    io.emit('notify', msg);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});