Notification sender & receiver
=====================================

Steps for setting the application up:

1. Install Node.js on the server
   https://nodejs.org/en/

2. Open the directory with this source code in the Terminal/Command Line and run
   npm install

3. Run on the server
   node app.js

4. Open the following URLs in separate browser tabs/windows/browsers
   http://localhost:3000/send
   http://localhost:3000/receive

5. Press the "Send notification" button in the browser

6. For using on a network with multiple devices replace "localhost" with the device IP/DNS name

7. Don't forget to enable port 3000 on the server's firewall

8. Use PM2 to run the Node.js application automatically when the server starts
   https://github.com/Unitech/pm2
